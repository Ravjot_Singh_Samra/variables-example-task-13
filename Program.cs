﻿using System;

namespace variables_example
{
    class Program
    {
        static void Main(string[] args)
        {
            var name = "Ravjot"; //string variable with my name
            
            Console.WriteLine($"Your name is {name}");
        }
    }
}
